from taskdash import result



{'name': 'testcase name',
 'status': 'PASS',
 'subresults': [
     {'name': 'sub1',
      'status': 'PASS',
      'required': False
      },
     {'name': 'sub2',
      'status': 'FAILED',
      'required': True
      }]}

class TestResultComputation(object):

#    def setupMethod(self, cls):
#        cls.sample_dict = {'name': 'trivial test',
#                'testcases': [ {'name': 'foo',
#                                'required': True},
#                               {'name': 'bar',
#                                'required': False},
#                               {'name': 'baz',
#                                'required': False}
#                              ]}

    def test_compute_one_pass(self):

        ref_dict = {'name': 'trivial test',
                    'testcases': [ {'name': 'foo', 'required': True}]}

        results = result.calculate_results()
